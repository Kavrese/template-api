from flask import jsonify, request, url_for, g, abort, app
from . import bp
from .errors import error_response_model


@bp.route('/test', methods=['POST'])
def test():
    return jsonify({
        "test": "YES"
    })
