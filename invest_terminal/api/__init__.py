from flask import Blueprint
from invest_terminal.utils import connect_to_db
from .request_to_db import RequestDB

bp = Blueprint('api', __name__)
conn = connect_to_db()
request_db = RequestDB(conn)

from . import api